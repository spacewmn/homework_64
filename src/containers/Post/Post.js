import React, {useEffect, useState} from 'react';
import axiosBlog from "../../axiosBlog";
import {NavLink} from "react-router-dom";

const Post = props => {
    // console.log(props.match.params);
    const [post, setPost] = useState(null);

    useEffect(() => {
        const getPostsList = async () => {
            const postResponse = await axiosBlog.get('/blog/' + props.match.params.id + '.json');
            setPost(postResponse.data);
        };

        getPostsList().catch(console.error);
    }, [props.match.params.id]);

    const deletePost = async id => {
        await axiosBlog.delete('/blog/' + id + '.json');
        props.history.push('/');
    };



    return post && (
        <div className="container">

            <div className="card mt-5">
                <div className="card-header">
                    Post
                </div>
                <div className="card-body">
                    <h5 className="card-title">{post.title}</h5>
                    <p className="card-text">{post.description}</p>
                    <button onClick={() => deletePost(props.match.params.id)} className="btn btn-danger mr-3">Delete</button>
                    <NavLink to={"/post/" + props.match.params.id + "/edit"} className="btn btn-primary">Change</NavLink>
                </div>
            </div>
        </div>
    );
};

export default Post;