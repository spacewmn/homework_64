import React, {useEffect, useState} from 'react';
import axiosBlog from "../../axiosBlog";

const FullPost = props => {

    const [newTitle, setNewTitle] = useState('');
    const [newDescription, setNewDescription] = useState('');

    useEffect(() => {
        const getPostsList = async () => {
            const postResponse = await axiosBlog.get('/blog/' + props.match.params.id + '.json');
            setNewTitle(postResponse.data.title);
            setNewDescription(postResponse.data.description);
        };

        getPostsList().catch(console.error);
    }, [props.match.params.id]);

    const changeTitle = event => {
        setNewTitle(event.target.value);
    };

    const changeDescription = event => {
        setNewDescription(event.target.value);
    };

    const changePost = async (e, id)  => {
        e.preventDefault()
        await axiosBlog.put('/blog/' + id + '.json', {
            title: newTitle,
            description: newDescription
        });
        props.history.push('/');
    };

    return (
        <div className="container">
            <form className="form-group my-5">
                <div className="form-group">
                    <label>Title</label>
                    <input
                        value={newTitle}
                        type="text"
                        name="title"
                        className="form-control"
                        onChange={changeTitle}
                        required
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="exampleFormControlTextarea1">Description</label>
                    <textarea
                        value={newDescription}
                        className="form-control"
                        name="description"
                        id="exampleFormControlTextarea1"
                        rows="3"
                        onChange={changeDescription}
                        required={true}
                    />
                </div>
                <button type="submit" onClick={(event) => changePost(event, props.match.params.id)} className="btn btn-primary">Save</button>
            </form>
        </div>
    );
};

export default FullPost;