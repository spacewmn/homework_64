import React, {useState} from 'react';
import axiosBlog from "../../axiosBlog";

const Add = props => {
    const [loading, setLoading] = useState(false);
    const [post, setPost] = useState({
        title: '',
        description: ''
    });

    const onChangeHandler = event => {
        const name = event.target.name;
        const value = event.target.value;

        setPost({...post, [name]: value});
    };

    const postHandler = async event => {
        event.preventDefault();
        setLoading(true);
        try {
            await axiosBlog.post('/blog.json', post);
        } finally {
            setLoading(false);
            props.history.push('/');
        };
    };

    let form = (
            <form className="form-group my-5" onSubmit={postHandler}>
                <div className="form-group">
                    <label>Title</label>
                    <input
                        type="text"
                        name="title"
                        className="form-control"
                        onChange={onChangeHandler}
                        required
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="exampleFormControlTextarea1">Description</label>
                    <textarea
                        className="form-control"
                        name="description"
                        id="exampleFormControlTextarea1"
                        rows="3"
                        onChange={onChangeHandler}
                        required={true}
                    />
                </div>
                <button type="submit" className="btn btn-primary">Add post</button>
            </form>
    );

    if (loading) {
        return (
            <div className="text-center mt-5">
                <div className="spinner-border" role="status">
                    <span className="sr-only">Loading...</span>
                </div>
            </div>
        );
    };

    return (
        <div className="container">
            {form}
        </div>
    );
};

export default Add;