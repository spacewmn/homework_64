import React, {useEffect, useState} from 'react';
import axiosBlog from "../../axiosBlog";
import {NavLink} from "react-router-dom";

const Home = () => {
    const [posts, setPosts] = useState([]);

    useEffect(() => {
        const getPostsList = async () => {
            const postResponse = await axiosBlog.get('/blog.json');
            setPosts(postResponse.data);
        };
        getPostsList().catch(console.error);
    }, []);

    const dataPost = Object.keys(posts).map((key) => {
        return (
            <div className="card my-5"
            key={key}>
                <div className="card-header">
                    Post
                </div>
                <div className="card-body">
                    <h5 className="card-title">{posts[key].title}</h5>
                    <NavLink className="btn btn-primary" to={'/post/' + key}>Read more ></NavLink>
                </div>
            </div>
        );
    });

    return (
        <div className="container">
            {dataPost}
        </div>
    );
};

export default Home;