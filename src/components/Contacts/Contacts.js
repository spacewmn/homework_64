import React from 'react';

const Contacts = () => {
    return (
        <div className="container">
            <div className="card">
                <img src="https://sputnik.kg/images/104752/92/1047529221.jpg" className="card-img-top" alt="..."/>
                    <div className="card-body">
                        <h1>Our contacts</h1>
                        <p className="card-text">Some quick example text to build on the card title and make up the bulk
                            of the card's content.</p>
                        <ul className="list-group">
                            <li className="list-group-item">0 555 14 15 16</li>
                            <li className="list-group-item">0 777 77 78 77</li>
                            <li className="list-group-item">0 700 00 11 22</li>
                            <li className="list-group-item">www.myblog.com</li>
                        </ul>
                    </div>
            </div>
        </div>
    );
};

export default Contacts;