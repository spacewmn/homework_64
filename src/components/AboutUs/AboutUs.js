import React from 'react';

const AboutUs = () => {
    return (
        <div className="container">
            <div className="card mt-3" >
                <img src="https://s3.tproger.ru/uploads/2019/04/learn-programming-880x308.png" className="card-img-top" alt="..."/>
                    <div className="card-body">
                        <h1>About Us</h1>
                        <p className="card-text">Some quick example text to build on the card title and make up the bulk
                            of the card's content. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos dolores ea fugit maxime sapiente suscipit ullam. Dolorem ea esse inventore minima perferendis quae rem sed sunt unde ut. Amet aperiam, architecto asperiores cum dolores inventore ipsam ipsum laborum mollitia, praesentium, repellendus reprehenderit. Animi eligendi exercitationem harum iure, iusto molestias necessitatibus.</p>
                    </div>
            </div>
        </div>
    );
};

export default AboutUs;