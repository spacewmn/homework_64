import React from 'react';
import './App.css';
import {Route, Switch, BrowserRouter, NavLink} from 'react-router-dom';
import AboutUs from "./components/AboutUs/AboutUs";
import Home from "./containers/Home/Home";
import Contacts from "./components/Contacts/Contacts";
import Add from "./containers/Add/Add";
import FullPost from "./containers/FullPost/FullPost";
import Post from "./containers/Post/Post";

const App = () => {

  return (
      <BrowserRouter>
          <div className="container">
              <nav className="navbar navbar-expand-lg navbar-light bg-light">
                  <div className="collapse navbar-collapse" id="navbarNav">
                      <ul className="navbar-nav">
                          <li className="nav-item">
                              <NavLink className="nav-link" to="/">Home</NavLink>
                          </li>
                          <li className="nav-item">
                              <NavLink className="nav-link" to={"/add"}>Add</NavLink>
                          </li>
                          <li className="nav-item">
                              <a className="nav-link" href={"/about"}>About</a>
                          </li>
                          <li className="nav-item">
                              <a className="nav-link" href={"/contacts"}>Contacts</a>
                          </li>
                      </ul>
                  </div>
              </nav>
          </div>
        <Switch>
          <Route path="/" exact component={Home}/>
          <Route path="/post/:id" exact component={Post}/>
          <Route path="/post/:id/edit" component={FullPost}/>
          <Route path="/contacts" component={Contacts}/>
          <Route path="/add" component={Add}/>
          <Route path="/about" component={AboutUs}/>
          <Route render={() => <h1>404 Not Found</h1>}/>
        </Switch>
      </BrowserRouter>
  );
}

export default App;
