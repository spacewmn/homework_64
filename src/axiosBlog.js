import axios from 'axios';

const axiosBlog = axios.create({
    baseURL: 'https://burger-project-js7.firebaseio.com'
});

export default axiosBlog;